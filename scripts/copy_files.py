import argparse
import shutil
from pathlib import Path
from . import MOD_PATH, check_cwd_is_correct

PUBLIC_PATH = Path("binaries/data/mods/public/")

DEFAULT_COPY = [
	"simulation/data",
	"simulation/templates"
]

def validate_path(path: str):
	mod_path = Path(path) / PUBLIC_PATH
	try:
		with open(mod_path / "mod.json", "r") as f:
			mod = json.load(f)
			if mod['name'] != "0ad":
				raise Exception("mod.json has incorrect name")
	except:
		raise Exception(f"path '{path}' does not point to a 0 A.D. SVN folder.")
	return mod_path


def copy_0ad_files(path_0ad: Path, to_copy = DEFAULT_COPY):
	for path in to_copy:
		shutil.copytree(path_0ad / path, MOD_PATH / path, dirs_exist_ok=False)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Copy files from 0 A.D. to the community mod.')
	parser.add_argument('-0ad', help='Path to the 0 A.D. folder')
	parser.add_argument('-p','--path', nargs='*', help='Optionally, a list of paths to copy.')

	args = parser.parse_args()

	check_cwd_is_correct()
	path = validate_path(getattr(args, '0ad'))
	copy_0ad_files(path, args.folder or DEFAULT_COPY)
else:
	raise Exception("Must be called directly")

