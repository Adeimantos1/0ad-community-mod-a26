# 0 A.D. Community mod for Alpha 26 Zhuangzi [![Latest Release](https://gitlab.com/wraitii/0ad-community-mod-a26/-/badges/release.svg)](https://gitlab.com/wraitii/0ad-community-mod-a26/-/releases)

This is a community-powered mod for 0 A.D.'s alpha 26, officially supported by the core team. The intention is to make it possible to improve the A26 gameplay experience, particularly MP balance.
The team wanted to give the community more power, and make it easier to contribute, thus this is hosted on gitlab and community members can request commit access.

## For maintainers with commit access

You are responsible for this mod. When rewiewing merge requests and changes, consider the following:
- This is not intended as an overhaul mod but as tweaks on the A26 formula to make it more enjoyable.
- Changes should be incremental so they can be play-tested.
- You may want to merge risky changes, they can be reverted a bit later.
- Feel free to ask the core team for support.

Please read 'releasing new versions' below as well.

## For contributors making merge requests

First of all, thanks for contributing to the mod. Please:
- Be mindful of committer's time and energy.
- Make small changes that can be easily reviewed.
- Take this as a potential learning experience for the 0 A.D. gitlab migration in the future

## For players of the mod

Actually playing this mod is the key to sucess. Please:
- Report on changes in the merge requests or open new issues.
- Be respectful of other's work. Not every change will work as intended.

### Releasing new versions

The repository is setup so that increasing the version number in `mod.json` will create a new version of the mod that can be downloaded directly. Please do so at reasonable intervals, so that people can easily test the mod that way.

The 0 A.D. team will also attempt to sign versions regularly, releasing them on mod.io so that the mod remains reasonable up-to-date.

### Repository management

Anyone can make Merge Requests and suggest changes. Getting commit access is possible, but you will need to request it to the 0 A.D. core team. To avoid spam and keep things manageable, we reserve the right to grant / rescind commit access at any time.

### Scope

The mod starts out with the simulation `data/` and `templates/` folders.
This makes it easier to change these files without copying anything explicitly.

It is possible to use the copy_files script to copy new files, if needed, e.g:
`python3 -m scripts.copy_files -0ad="[path]" -p "maps/random"`

If you do add new files, please first add the unmodified file from A26 then apply your changes on top of it in a separate commit. This will make the reviewers' jobs much easier.
